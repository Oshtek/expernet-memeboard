package com.example.memeboard;

import android.media.MediaExtractor;
import android.media.MediaPlayer;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    ImageButton btnbruh,btncoffin,btndababy,btneeee,btntwenty1,btnwhee;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainpage);
        btnbruh = (ImageButton)findViewById(R.id.bruhbtn);
        btnbruh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.bruh);
                boolean test1 = mediaPlayer.isPlaying();
                Log.d("Avant", String.valueOf(test1));
                mediaPlayer.start();
                boolean test2 = mediaPlayer.isPlaying();
                Log.d("Apres", String.valueOf(test2));
            }

        });
        btncoffin = (ImageButton) findViewById(R.id.coffinbtn);
        /*btncoffin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.coffin);
                try {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
            }
        });*/
        btndababy = (ImageButton) findViewById(R.id.dababybtn);
        /*btndababy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.dababy);
                try {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
            }
        });*/
        btneeee = (ImageButton) findViewById(R.id.eeeebtn);
        /*btneeee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.eeee);
                try {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
            }
        });*/
        btntwenty1 = (ImageButton) findViewById(R.id.twenty1btn);
        /*btntwenty1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.twenty1);
                try {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
            }
        });*/
        btnwhee = (ImageButton) findViewById(R.id.wheebtn);
        /*btnwhee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.whee);
                try {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
            }
        });*/

    }

    public void onClickbtn(View v) {
        setContentView(R.layout.menuajout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if ( id == android.R.id.home ) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public void onClick(View v) {

    }
}