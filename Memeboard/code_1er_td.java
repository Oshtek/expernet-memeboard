package re.expernet.td_android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener, TextToSpeech.OnInitListener{

    ArrayList<Personne> list=new ArrayList<Personne>();
    EditText nom,prenom,phone;
    Spinner monSpinner;

    @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("CYCLEDEVIE", "Create");

        setContentView(R.layout.test);

        monSpinner = (Spinner) this.findViewById(R.id.monSpinner);

    }
    TextToSpeech tts;

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("CYCLEDEVIE", "Start");
        tts=new TextToSpeech(this,this);

    }

    @Override
    public void onInit(int i){
        tts.speak("Bonjour",TextToSpeech.QUEUE_ADD,null,"1");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("CYCLEDEVIE", "Resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("CYCLEDEVIE", "Pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("CYCLEDEVIE", "Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("CYCLEDEVIE", "Destroy");
    }

    public void click(View v){
        Log.d("click", "and collect");
        Toast.makeText(this,"Click",Toast.LENGTH_LONG).show();
        Intent reco=new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        reco.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        );

        startActivityForResult(reco,1);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        TextView infos=(TextView) this.findViewById(R.id.summary);
        Personne contact= (Personne) monSpinner.getSelectedItem();
        infos.setText("Nom :" + contact.getNom() + " Prenom : " + contact.getPrenom() + "Telephone : " + contact.getTelephone());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void ajoutContact(View c) {

        EditText nom = (EditText) this.findViewById(R.id.formNom);
        EditText prenom = (EditText) this.findViewById(R.id.formPrenom);
        EditText phone = (EditText) this.findViewById(R.id.formPhone);

        String[] contacts =new String[this.list.size()+1];

        ListAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,contacts);

        ((ArrayAdapter)adapter).setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        monSpinner.setAdapter((SpinnerAdapter)adapter);


        Personne personne = new Personne(nom.getText().toString(),prenom.getText().toString(),phone.getText().toString());
        this.list.add(personne);

        nom.setText(" ");
        prenom.setText(" ");
        phone.setText(" ");

        for (int i = 0 ; i < this.list.size(); i++) {
            contacts[i] = list.get(i).getPrenom() + " " + list.get(i).getNom() + " " + list.get(i).getTelephone();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
        Toast.makeText(this," "+resultCode,Toast.LENGTH_LONG).show();
    }

}
